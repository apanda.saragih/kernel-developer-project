#!/bin/sh
read -p "INPUT : What do you want to do? ( record video / exit )" DECIDE
case "$DECIDE" in
        [Rr][Ee][Cc][Oo][Rr][Dd][" "][Vv][Ii][Dd][Ee][Oo])
                echo "\n= = = = = = = S T A R T   R E C O R D I N G = = = = = = ="
                echo "press Ctrl + C to stop recording"
                python3 record.py
                ;;
        [Ee][Xx][Ii][Tt])
                echo "Exit . . ."
                ;;
        *)
                echo "Please input appropriate Input, either \"record video\" or \"exit\" "
                echo "Exit . . ."
esac
